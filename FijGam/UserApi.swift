//
//  UserApi.swift
//  FijGam
//
//  Created by Brian Mukandiwa on 06/12/2019.
//  Copyright © 2019 Brian Mukandiwa. All rights reserved.
//

import Foundation
public struct UserApi: UserApiProtocol {
    public static func getUser(getCurrentUsers: Users, callback: @escaping (Users?, ErrorResponse?) -> Void) {
        let baseUrl = Api.baseUrl
        
        let URLString = baseUrl
        
        Networking<Users>
            .call(url: URLString)
            { apiResponse in
                
                if (apiResponse?.success)! {
                    guard let data = apiResponse?.data as? Users else {
                        callback(nil, Util.callbackErrorResponse(errorResponse: apiResponse?.error, errorCode: "091", errorDisplay: "An error occurred"))
                        return
                    }
                    if !((data != nil)) {
                        
                        
                    }
                    callback(data, nil)
                } else {
                    callback(nil, Util.callbackErrorResponse(errorResponse: apiResponse?.error, errorCode: "091", errorDisplay: "An error occurred"))
                }
                
        }    }
    
    
    
    
    

    
    func createWeatherObjectWith(json: Data, completion: @escaping (_ data: Users?, _ error: Error?) -> Void) {
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let weather = try decoder.decode(Users.self, from: json)
            return completion(weather, nil)
        } catch let error {
            print("Error creating current weather from JSON because: \(error.localizedDescription)")
            return completion(nil, error)
        }
    }
    
    
    
    
    
    
    
}


