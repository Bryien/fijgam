//
//  Util.swift
//  FijGam
//
//  Created by Brian Mukandiwa on 06/12/2019.
//  Copyright © 2019 Brian Mukandiwa. All rights reserved.
//

import Foundation
public struct Util {
public static let USERS = "SyncInfo.USERS"
    

    
    
    public static func callbackErrorResponse(errorResponse: ErrorResponse?, errorCode: String?, errorDisplay: String?) -> ErrorResponse {
        if errorResponse != nil {
            return errorResponse!
        }
        var temp = ErrorResponse()
        temp.error = errorCode
        temp.errorCode = errorCode
        temp.description = "An unexpected error occurred"
        temp.errorDescription = "An unexpected error occurred"
        temp.errorDisplay = errorDisplay
        return temp
        
    }
    
    public static func toHumanReadableDate(date: Date) -> (date: String, time: String, dateTime: String)? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM, yyyy"
        let timeFormtter = DateFormatter()
        timeFormtter.dateFormat = "HH:mm"
        let dateTimeFormtter = DateFormatter()
        dateTimeFormtter.dateFormat = "dd-MM-yyyy HH:mm"
        
        let dateStr = dateFormatter.string(from: date)
        let timeStr = timeFormtter.string(from: date)
        let dateTimeStr = dateTimeFormtter.string(from: date)
        
        return (date: dateStr, time: timeStr, dateTime: dateTimeStr)
    }
    
    public static func toDate(string: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
        return dateFormatter.date(from: string)
    }
    
    
    public static func stringToFloat(string: String?) -> Float? {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return numberFormatter.number(from: string!)?.floatValue
    }
}
