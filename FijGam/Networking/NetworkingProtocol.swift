//
//  NetworkingProtocol.swift
//  FijGam
//
//  Created by Brian Mukandiwa on 06/12/2019.
//  Copyright © 2019 Brian Mukandiwa. All rights reserved.
//

import Foundation
public protocol NetworkingProtocol {
    
    static func call(url: String, callback: @escaping (ApiResponse?) -> Void)
    
}
