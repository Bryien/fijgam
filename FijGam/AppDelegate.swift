//
//  AppDelegate.swift
//  FijGam
//
//  Created by Brian Mukandiwa on 06/12/2019.
//  Copyright © 2019 Brian Mukandiwa. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    internal func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//        window = UIWindow(frame: UIScreen.main.bounds)
//        window?.makeKeyAndVisible()
//        let layout =    UICollectionViewFlowLayout()
//        let nc =    UINavigationController(rootViewController : HomeViewController())
//        window?.rootViewController = nc
//      UINavigationBar.appearance().backgroundColor = UIColor(red: 141 , green: 197, blue: 65, alpha: 0)
        FirebaseApp.configure()
        Database.database().isPersistenceEnabled = true
   
        return true
    }
}
