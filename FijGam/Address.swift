//
//  Address.swift
//  FijGam
//
//  Created by Brian Mukandiwa on 06/12/2019.
//  Copyright © 2019 Brian Mukandiwa. All rights reserved.
//


import Foundation
struct  Address: Codable {
    var street: String?
    var suite: String?
    var city: String?
    var zipcode: String?
    var geo: Geo?
    public init(){}
    
}
