//
//  DaoProtocol.swift
//  FijGam
//
//  Created by Brian Mukandiwa on 06/12/2019.
//  Copyright © 2019 Brian Mukandiwa. All rights reserved.
//

import Foundation
public protocol DaoProtocol {
    
    static func get(url: String, callback: @escaping (Codable?) -> Void)
    
    static func post(url: String, data: Codable, callback: @escaping (Codable?) -> Void)
    
    static func put(url: String, data: Codable, callback: @escaping (Codable?) -> Void)
    
    static func remove(url: String, callback: @escaping (Codable?) -> Void)
    
    static func clear(callback: @escaping (Bool?) -> Void)
    
}
