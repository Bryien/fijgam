//
//  SyncInfoDao.swift
//  FijGam
//
//  Created by Brian Mukandiwa on 06/12/2019.
//  Copyright © 2019 Brian Mukandiwa. All rights reserved.
//

import Foundation
public struct SyncInfoDao<T: Codable>: DaoProtocol {
    
    public static func get(url: String, callback: @escaping (Codable?) -> Void) {
        let data = UserDefaults.standard.value(forKey: url)
        guard data != nil else {
            callback(nil)
            return
        }
        do {
            callback(try JSONDecoder().decode(T.self, from: data as! Data))
        }catch {
            fatalError(error.localizedDescription)
        }
    }
    
    public static func post(url: String, data: Codable, callback: @escaping (Codable?) -> Void) {
        
        let encoder = JSONEncoder()
        
        do {
            let objectToSave: T = data as! T
            let json = try encoder.encode(objectToSave)
            
            UserDefaults.standard.set(json, forKey: url)
            
            callback(data)
        }catch {
            fatalError(error.localizedDescription)
        }
    }
    
    public static func put(url: String, data: Codable, callback: @escaping (Codable?) -> Void) {
        let encoder = JSONEncoder()
        
        do {
            let objectToSave: T = data as! T
            let json = try encoder.encode(objectToSave)
            
            UserDefaults.standard.set(json, forKey: url)
            
            callback(data)
        }catch {
            fatalError(error.localizedDescription)
        }
    }
    
    public static func remove(url: String, callback: @escaping (Codable?) -> Void) {
        
        let data = UserDefaults.standard.value(forKey: url)
        
        UserDefaults.standard.removeObject(forKey: url)
        
        do {
            callback(try JSONDecoder().decode(T.self, from: data as! Data))
        } catch {
            // log?.debug(error.localizedDescription)
        }
    }
    public static func clear(callback: @escaping (Bool?) -> Void) {
        if let bundleId = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: bundleId)
        }
    }
    
}
