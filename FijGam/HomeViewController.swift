//
//  HomeViewController.swift
//  FijGam
//
//  Created by Brian Mukandiwa on 07/12/2019.
//  Copyright © 2019 Brian Mukandiwa. All rights reserved.
//

import Foundation
import  UIKit
import Firebase



class HomeViewController: UITableViewController {
    var dataSource: [Users] = []
   
    

    
 override func viewDidLoad() {
        super.viewDidLoad()

         navigationItem.title = ""
         navigationController?.navigationBar.barTintColor = .darkGray
    
    
    let btnRefresh = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.refresh, target: self, action: #selector(reload))
    
      navigationItem.leftBarButtonItem = btnRefresh
    
     navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add More Users", style: .plain, target: self, action: #selector(addTapped))


    getUsers(tableView: tableView)
    let jsonUrlString = "https://jsonplaceholder.typicode.com/users"
    guard let url = URL(string: jsonUrlString) else { return }
    
    URLSession.shared.dataTask(with: url) { (data, response, err) in
        
        
        guard let data = data else { return }
        
        
        do {
            
            
            self.dataSource = try JSONDecoder().decode([Users].self, from: data)
            print(self.dataSource)
            self.tableView.register(CustomViewCell.self, forCellReuseIdentifier: "cell")

            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        } catch let jsonErr {
            print("Error serializing json:", jsonErr)
        }
        
        
        
        }.resume()
        
        
    }
    
  override  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath as IndexPath) as! CustomViewCell
        let user = dataSource[indexPath.row]

        cell.email.text = user.name
        cell.name.text = user.email
        cell.username.text = user.username
        
        return cell
    }
        
    
    

    
    func getUsers(tableView : UITableView)  {
  
      

    }
    
    @objc func addTapped()  {
        let newViewController = LoginViewController()
        self.navigationController?.pushViewController(newViewController, animated: true)
        print("l was clicked ")
        
        FirebaseApp.configure()
        Database.database().isPersistenceEnabled = true
        
        }
    
    @objc func reload(){
    print("reload....")
    self.tableView.reloadData();
    }
}




    
    
    
    
    

