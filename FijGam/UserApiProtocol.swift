//
//  UserApiProtocol.swift
//  FijGam
//
//  Created by Brian Mukandiwa on 06/12/2019.
//  Copyright © 2019 Brian Mukandiwa. All rights reserved.
//

import Foundation
public protocol UserApiProtocol {
static func getUser(getCurrentUsers: Users, callback: @escaping (Users?, ErrorResponse?) -> Void)
}
