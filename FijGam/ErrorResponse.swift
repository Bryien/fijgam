//
//  ErrorResponse.swift
//  FijGam
//
//  Created by Brian Mukandiwa on 06/12/2019.
//  Copyright © 2019 Brian Mukandiwa. All rights reserved.
//

import Foundation
public struct ErrorResponse: Codable {
    public var error: String?
    public var errorDescription: String?
    public var errorDisplay: String?
    public var description: String?
    public var errorCode: String?
    
    public init() {
        
    }
}
