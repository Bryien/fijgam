//
//   ApiResponse.swift
//  FijGam
//
//  Created by Brian Mukandiwa on 06/12/2019.
//  Copyright © 2019 Brian Mukandiwa. All rights reserved.
//

import Foundation
public struct ApiResponse {
    public let success: Bool
    public let data: Any?
    public let error: ErrorResponse?
}
